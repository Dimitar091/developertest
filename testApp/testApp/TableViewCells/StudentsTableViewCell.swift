//
//  StudentsTableViewCell.swift
//  testApp
//
//  Created by Dimitar on 21.4.21.
//

import UIKit

class StudentsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGrade: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupCell(students: Students) {
       if let name = students.name {
           lblName.text = name
       }
       if let grade = students.grade {
        lblGrade.text = "Grade: ".localized() + "\(grade)"
       }
   }
}
