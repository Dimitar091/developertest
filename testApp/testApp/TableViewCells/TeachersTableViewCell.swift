//
//  TeachersTableViewCell.swift
//  testApp
//
//  Created by Dimitar on 20.4.21.
//

import UIKit
import Kingfisher

class TeachersTableViewCell: UITableViewCell {

    @IBOutlet weak var teacherImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblClass: UILabel!
    @IBOutlet weak var btnContact: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    
    func setupCell(teachers: Teacher) {
        btnContact.setTitle(NSLocalizedString("btnContact", comment: ""), for: .normal)
        if let name = teachers.name {
            lblName.text = name
        }
        if let classSubject = teachers.classSubject {
            lblClass.text = "Class: ".localized() + classSubject
        }
            guard let imageUrl = teachers.imageUrl else { return }
            self.teacherImage.kf.setImage(with: URL(string: imageUrl))
    }
    
}
