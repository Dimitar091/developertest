//
//  Endpoint.swift
//  CoronaVirusApp
//
//  Created by Dimitar on 19.4.21.
//

import Foundation
import UIKit


protocol Endpoint  {
    // http or https
    var scheme: String { get }
    
    // BASE URL(zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev)
    var host: String { get }
    
    // Instead of URL we have Request becouse we will use URLSession
    var request: URLRequest? { get }
    
    // GET, POST, DELETE, PUT
    var httpMethod: String { get }

    // Content-type:application/json...
    var httpsHeaders: [String: String]? { get }
    
    // Body of the request (parameters) json
    var body: [String: Any]? { get }
    
    // URL parameters (&itemOne=valueOne&itemTwo=valueTwo)
    var queryItems: [URLQueryItem]? { get }
}

extension Endpoint {
    
    var scheme: String {
        return "https"
    }
    
    var host: String {
        return "zpk2uivb1i.execute-api.us-east-1.amazonaws.com"
    }
}

// scheme://host/path?qurtyItems
extension Endpoint {
    func request(forEndpoint path: String) -> URLRequest? {
        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = host
        urlComponents.path = path
        urlComponents.queryItems = queryItems
        guard let url = urlComponents.url else { return nil }
        var request = URLRequest(url: url)
        // Set http method
        request.httpMethod = httpMethod
        // Set http headers if any
        if let httpHeaders = httpsHeaders {
            httpHeaders.forEach({ (key, value) in
                request.setValue(value, forHTTPHeaderField: key)
            })
        }
        // Set the request Body
        if let body = body {
            let jsonData = try? JSONSerialization.data(withJSONObject: body)
            request.httpBody = jsonData
        }
        
        return request
    }
}
