//
//  WebService.swift
//  CoronaVirusApp
//
//  Created by Dimitar on 19.4.21.
//

import Foundation

typealias ResultsCompletion<T> = (Result<T, Error>) -> Void

protocol WebServiceProtocol {
    func request<T: Decodable>(_ endpoint: Endpoint, completion: @escaping ResultsCompletion<T>)
}

class WebService: WebServiceProtocol {
    let urlSession: URLSession
    private let parser: Parser
    
    init(urlSession: URLSession = URLSession(configuration: URLSessionConfiguration.default), parser: Parser = Parser()) {
        self.urlSession = urlSession
        self.parser = parser
    }
    
    func request<T: Decodable>(_ endpoint: Endpoint, completion: @escaping ResultsCompletion<T>) {
        guard let request = endpoint.request else {
            print("Request is nil")
            return
        }
        let task = urlSession.dataTask(with: request) { (data, _, error) in
            if let error = error {
                OperationQueue.main.addOperation {
                    completion(.failure(error))
                }
                return
            }
            guard let data = data else {
                print("Missing data")
                return
            }
            self.parser.json(data: data, completion: completion)
        }
        task.resume()
    }
}

struct Parser {
    private let jsonDecoder = JSONDecoder()
    
    func json<T: Decodable>(data: Data, completion: @escaping ResultsCompletion<T>) {
        do {
            let result = try jsonDecoder.decode(T.self, from: data)
            OperationQueue.main.addOperation {
                completion(.success(result))
            }
        } catch  {
            OperationQueue.main.addOperation {
                completion(.failure(error))
            }
        }
    }
}
