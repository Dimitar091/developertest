//
//  SchoolApi.swift
//  testApp
//
//  Created by Dimitar on 22.4.21.
//

import Foundation

enum SchoolAPI: Endpoint {
    case getSchool(_ schoolId: Int)
    case getTeacherDetails(teachers: Teacher)
    case getStudentDetails(students: Students)
    
    //zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev
    //https://zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev/schools/{id}
    //https://zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev/teachers/{id}
    //https://zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev/students/{id}
    var request: URLRequest? {
        switch self {
        case .getSchool(let schoolId):
            return request(forEndpoint: "/dev/schools/\(schoolId)")
        case .getTeacherDetails(teachers: let teacher):
            return request(forEndpoint: "/dev/teachers/\(teacher.id)")
        case .getStudentDetails(students: let student):
            return request(forEndpoint: "/dev/students/\(student.id)")
        }
    }
    
    var httpMethod: String {
        switch self {
        case .getSchool,
             .getTeacherDetails,
             .getStudentDetails:
            return "GET"
        }
    }
    
    var httpsHeaders: [String : String]? {
        return nil
    }
    
    var body: [String : Any]? {
        return nil
    }
    
    var queryItems: [URLQueryItem]? {
        return nil
    }

}
