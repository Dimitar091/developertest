//
//  DetailsViewController.swift
//  testApp
//
//  Created by Dimitar on 21.4.21.
//

import UIKit
import Kingfisher

class DetailsViewController: UIViewController {

    
    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var downView: UIView!
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var schoolImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblClass: UILabel!
    @IBOutlet weak var lblSchool: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblAboutDesc: UILabel!
    
    
    var teacher: Teacher?
    private let api = WebService()
    var school: School?
    var student: Students?


    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        if let teacher = teacher {
            btnContact.isHidden = false
            getShoolDetailsFor(teacher.schoolId)
        } else if let student = student {
            btnContact.isHidden = true
            getShoolDetailsFor(student.schoolId)
        }
    }
    
    private func setupView() {
        self.view.backgroundColor = UIColor(red: 0.898, green: 0.898, blue: 0.898, alpha: 1)
        downView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        downView.layer.shadowOpacity = 1
        downView.layer.shadowOffset = CGSize(width: 2, height: 2)
        downView.layer.shadowRadius = 10

        upView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        upView.layer.shadowOpacity = 1
        upView.layer.shadowOffset = CGSize(width: 2, height: 2)
        upView.layer.shadowRadius = 10
        
        downView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        upView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    func setupTeacherInfo(_ school: School) {
        lblName.text = teacher?.name
        lblClass.text = teacher?.classSubject
        guard let schoolName = school.name else { return }
        lblSchool.text = "School: ".localized() + schoolName
        guard let imageUrl = school.schoolImg else { return }
            self.schoolImage.kf.setImage(with: URL(string: imageUrl))
        if let classSubject = teacher?.classSubject  {
            lblClass.text = "Class: ".localized() + classSubject
        }
        if let description = teacher?.description {
            lblAboutDesc.text = description
        }
    }
    
    func setupSchool(_ school: School) {
        if let name = student?.name {
            lblName.text = name
        }
        if let grade = student?.grade {
            lblClass.text = "Grade: ".localized() + "\(grade)"
        }
        guard let schoolName = school.name else { return }
        lblSchool.text = "School: ".localized() + schoolName
        guard let imageUrl = school.schoolImg else { return }
            self.schoolImage.kf.setImage(with: URL(string: imageUrl))
        if let description = student?.description {
            print(description)
            lblAboutDesc.text = description
        }
    }
    private func getShoolDetailsFor(_ id: Int) {
        api.request(SchoolAPI.getSchool(id)) { (_ result: Result<School,Error>) -> Void in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(let schoolDetails):
                self.setupTeacherInfo(schoolDetails)
                self.setupSchool(schoolDetails)

              }
            }
        }
    
    @IBAction func onContact(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message:NSLocalizedString("btnContact", comment: ""), preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title:NSLocalizedString("email", comment: ""), style: .default, handler: nil))
        alert.addAction(UIAlertAction(title:NSLocalizedString("message", comment: ""), style: .default, handler: nil))
        alert.addAction(UIAlertAction(title:NSLocalizedString("call", comment: ""), style: .default, handler: nil))
        alert.addAction(UIAlertAction(title:NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
}

