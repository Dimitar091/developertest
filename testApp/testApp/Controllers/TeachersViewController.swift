//
//  TeachersViewController.swift
//  testApp
//
//  Created by Dimitar on 19.4.21.
//

import UIKit

class TeachersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var teachers = [Teacher]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if Dev
        self.navigationItem.title = "Teachers - DEV".localized()

        #elseif Prod
        self.navigationItem.title = "Teachers".localized()
        
        #endif
        
        setupTableView()
        getTeachers()
        self.view.backgroundColor = UIColor(red: 0.898, green: 0.898, blue: 0.898, alpha: 1)
        tabBarController?.tabBar.barTintColor = UIColor(red: 0.898, green: 0.898, blue: 0.898, alpha: 1)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "teachersSegue",
            let destination = segue.destination as? DetailsViewController,
            let teacher = sender as? Teacher
        {
            destination.teacher = teacher
        }
    }
    private func setupTableView() {
        tableView.register(UINib(nibName: "TeachersTableViewCell", bundle: nil), forCellReuseIdentifier: "teachersCell")
        tableView.allowsSelection = true
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func getTeachers() {
        APIManager.shared.getTeachers { (teachers, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            } else {
                guard let teachers = teachers else { return }
                self.teachers = teachers
                self.tableView.reloadData()
            }
        }
    }
  }

extension TeachersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teachers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teachersCell") as! TeachersTableViewCell
        let teacher = teachers[indexPath.row]
        cell.setupCell(teachers: teacher)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let teacher = teachers[indexPath.row]
        performSegue(withIdentifier: "teachersSegue", sender: teacher)
        
    }
    
    
}
