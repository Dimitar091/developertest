//
//  StudentsViewController.swift
//  testApp
//
//  Created by Dimitar on 19.4.21.
//

import UIKit

class StudentsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var students = [Students]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        getStudents()
        self.view.backgroundColor = UIColor(red: 0.898, green: 0.898, blue: 0.898, alpha: 1)
        tabBarController?.tabBar.barTintColor = UIColor(red: 0.898, green: 0.898, blue: 0.898, alpha: 1)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "studentsSegue",
            let destination = segue.destination as? DetailsViewController,
            let student = sender as? Students
        {
            destination.student = student
        }
    }

    private func setupTableView() {
        tableView.register(UINib(nibName: "StudentsTableViewCell", bundle: nil), forCellReuseIdentifier: "studentsCell")
        tableView.allowsSelection = true
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
    private func getStudents() {
        APIManager.shared.getStudents { (students, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            } else {
                guard let student = students else { return }
                self.students = student
                self.tableView.reloadData()
            }
        }
    }
}

extension StudentsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentsCell") as! StudentsTableViewCell
        let student = students[indexPath.row]
        cell.setupCell(students: student)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let student = students[indexPath.row]
        performSegue(withIdentifier: "studentsSegue", sender: student)
        
    }
}

