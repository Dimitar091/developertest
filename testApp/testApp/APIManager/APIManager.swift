//
//  APIManager.swift
//  testApp
//
//  Created by Dimitar on 19.4.21.
//

import Foundation
import Alamofire


class APIManager {
    
    static let shared = APIManager()
    init() {}
    
    
    func getTeachers(completion: @escaping(_ teachers: [Teacher]?,_ error: Error? ) -> Void) {
        //https://zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev/teachers
        AF.request("https://zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev/teachers").responseDecodable(of: [Teacher].self) { response in
            if let error = response.error {
            print(error.localizedDescription)
                completion(nil,error)
        }
            if let teacher = response.value {
                completion(teacher, nil)
            }
        }
    }
    func getStudents(completion: @escaping(_ students: [Students]?,_ error: Error? ) -> Void) {
    AF.request("https://zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev/students").responseDecodable(of: [Students].self) { response in
        if let error = response.error {
        print(error.localizedDescription)
            completion(nil,error)
    }
        if let student = response.value {
            completion(student, nil)
        }
    }
  }
    
//    func getSchools(schoolId: Int, student: Int, teacher: Int, completion: @escaping(_ teachers: TeacherDetails?,_ error: Error? ) -> Void) {
//        AF.request("https://zpk2uivb1i.execute-api.us-east-1.amazonaws.com/dev/teachers/").responseDecodable(of: TeacherDetails.self) { response in
//            if let error = response.error {
//            print(error.localizedDescription)
//                completion(nil,error)
//        }
//            if let details = response.value {
//                completion(details, nil)
//            }
//        }
//    }
    
}

