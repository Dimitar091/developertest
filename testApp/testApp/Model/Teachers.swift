//
//  Teachers.swift
//  testApp
//
//  Created by Dimitar on 19.4.21.
//

import Foundation

struct Teacher: Codable {
    var id: Int
    var name: String?
    var schoolId: Int
    var classSubject: String?
    var imageUrl: String?
    var description: String?
    
    private enum CodingKeys: String, CodingKey {
        case schoolId = "school_id"
        case classSubject = "class"
        case imageUrl = "image_url"
        case id
        case name
        case description
    }
}
