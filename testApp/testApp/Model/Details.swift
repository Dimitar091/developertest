//
//  Details.swift
//  testApp
//
//  Created by Dimitar on 22.4.21.
//

import Foundation


struct School: Codable {
    var id: Int?
    var name: String?
    var schoolImg: String?
    
    private enum CodingKeys: String, CodingKey {
        case schoolImg = "image_url"
        case id
        case name
    }
}
