//
//  Students.swift
//  testApp
//
//  Created by Dimitar on 21.4.21.
//

import Foundation

struct Students: Codable {
    var id: Int
    var name: String?
    var grade: Int?
    var schoolId: Int
    var description: String?
    
    private enum CodingKeys: String, CodingKey {
        case schoolId = "school_id"
        case id
        case name
        case grade
        case description
    }
}
