//
//  Lozalizable+Extension.swift
//  testApp
//
//  Created by Dimitar on 25.4.21.
//

import Foundation
import UIKit

extension String {
    func localized() -> String {
         return NSLocalizedString(self,
                                  tableName: "Localizable",
                                  bundle: .main,
                                  value: self,
                                  comment: self)
    }
}
